/**
 * Programme principal - client.c
 *
 * Par : Nicolas Verreault & Briac Samson-tessier
 * Date : 2023-06-22
 *
 * Ce programme permet à l'utilisateur de rechercher des films, jeux vidéo et séries
 * dans une base de données IMDb. L'utilisateur peut spécifier différents critères tels que le titre,
 * la catégorie, l'année ou la période et le genre pour filtrer les résultats.
 * Les titres correspondants aux critères de recherche sont ensuite triés et enregistrés
 * dans un fichier TSV nommé "title_basics.tsv", tandis que les évaluations associées 
 * aux titres sont affichées dans le fichier "title_ratings.tsv".
 * 
 * Exemple d'exécution:
 * ./client -t "The Good" -g "Western" -a "1960:1970" -c "movie"
 */

#include "recherche.h"
#include "resultat.h"
#include "testunitaire.h"
#include "communication.h"
#include "imdb.h"

#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "unistd.h"
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//modifier TEST a 1 pour effectuer la batterie de test et a 0 pour les passer
#define TEST 0

int main(int argc, char *argv[]) 
{
    int option;
    char *titre = NULL;
    char *genre = NULL;
    char *annees = NULL;
    char *categorie = NULL;
    int evaluation = 0;

    // Parametres socket
    int socket_serveur;
    int socket_client;
    struct sockaddr_in server_addr;
    struct sockaddr_in client_addr;
    socklen_t server_addr_len;
    socklen_t client_addr_len;

    #if(TEST)
    // ************ ZONE DE TEST UNITAIRE ******************// 
    test_creer_detruire_critere();
    test_mutateurs_observateurs_critere();
    test_set_intervalle_annees();
    // *****************************************************//
    #endif

    // Lab2-HLR01 : Arguments provenant du terminal
    // Si aucun argument n'est donné, le programme s'arrête
    if (argc < 2) 
    {
        printf("\nAucun argument n'a été donné. Le programme va s'arrêter.\n");
        return EXIT_SUCCESS;
    }

    // Lab3-HLR02 : Arguments possibles avec getopt
    // Client-HLR03: Ajout d'un argument pour l'interface de vote
    while ((option = getopt(argc, argv, "t:g:a:c:v")) != -1) 
    {
        switch (option) 
        {
            case 't':
                titre = optarg;
                break;
            case 'g':
                genre = optarg;
                break;
            case 'a':
                annees = optarg;
                break;
            case 'c':
                categorie = optarg;
                break;
            case 'v':
                evaluation = 1;
                break;
            default:
                printf("\nVous devez minimalement specifier le titre du film.\n");
                printf("Le programme va maintenant s'arreter.\n");
                return EXIT_SUCCESS;
        }
    }

    printf("Envoi des criteres de recherches...\n\n");
    if (titre != NULL)
        printf("Titre: %s\n", titre);
    if (genre != NULL)
        printf("Genre: %s\n", genre);
    if (annees != NULL)
        printf("Annees: %s\n", annees);
    if (categorie != NULL)
        printf("Categorie: %s\n", categorie);
    if (evaluation == 1)
        printf("Demande d'evaluation du titre.\n");
    fflush(stdout);
    printf("\n");
    
    t_critere critere = creer_critere();
    set_titre(critere, titre);
    if (categorie) 
      set_categorie(critere, categorie);
    if (genre) 
      set_genre(critere, genre);
    if (annees) 
      set_intervalle_annees(critere, annees);  
    if (evaluation == 1) 
      set_evaluation_titre(critere, evaluation);

    // Comm-HLR01: Communication du serveur vers le client
    // *** SECTION INITIALISATION CLIENT SOCKET *** //
    // creation socket
    socket_client = socket(AF_INET, SOCK_STREAM, 0);

    // Activer l'option SO_REUSEADDR
    int optval = 1;
    if (setsockopt(socket_client, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) < 0) {
        perror("setsockopt(SO_REUSEADDR) failed");
        exit(1);
    }
    // creation IP:PORT information
    server_addr.sin_family = AF_INET;
    // Comm-HLR05: Communication du client vers le serveur
    // Comm-HLR06: Communication inter-hôte (optionnel)
    server_addr.sin_addr.s_addr = inet_addr("10.196.10.119");
    server_addr.sin_port = htons(PORT);
    server_addr_len = sizeof(server_addr);

    // connexion au serveur 
    // Comm-HLR04: Client sans serveur
    if (connect(socket_client, (struct sockaddr *)&server_addr, server_addr_len) == -1)
    {
        fprintf(stderr, "Erreur! il n'y a pas de serveur accessible. \n\n");
        close(socket_client);
        return EXIT_FAILURE;
    }

    // Comm-HLR01: Le client fait une opération de recherche
    envoyer_Critere_Recherche(critere, socket_client);

    // Comm-HLR04: Le client récupère les résultats d'une opération de recherche
    t_titreCherches resultat=reception_resultats_recherche(socket_client);

    TitresFichier(resultat);

    if (evaluation == 1) {
    fflush(stdout);
    fflush(stdin);
    int choix = -1;
    double nouvelle_note = -1;
    int resultat_nbTitres=get_nbTitres_resultat(resultat);

    // Comm-HLR05: Le client demande à l'utilisateur le titre à évaluer
    printf("\nVeuillez choisir un titre à évaluer (entre 1 et %d): ",resultat_nbTitres);
    scanf("%d", &choix);
    //fflush(stdin);

    if (choix >= 1 && choix <= resultat_nbTitres) {
        choix=choix-1;

        char* titreChoisi_primaryTitle = get_titre_resultat(resultat,choix);
        printf("\nVous avez choisi le titre : %s\n", titreChoisi_primaryTitle);

        // Comm-HLR06: Le client envoie au serveur le titre à évaluer
        envoie_index_Evaluation(choix, socket_client);

        // Comm-HLR09: Le client récupère la cote de classement du titre
        reception_cote_Evaluation(socket_client);
        printf("\nEntrez la nouvelle note : \n");
        scanf("%lf", &nouvelle_note);
        fflush(stdin);
        printf("Envoie de l'evaluation...\n");

        // Comm-HLR10: Le client envoie au serveur l'évaluation de l'utilisateur
        envoie_note_Evaluation(nouvelle_note, socket_client);
        printf("Nouvelle cote de classement recu\n");

        // Comm-HLR13: Le client récupère la nouvelle cote de classement du titre
        resultat = reception_resultats_recherche(socket_client);

        char* tconst_titre = get_tconst_resultat(resultat, choix);
        printf("\nTitre : %s\n", tconst_titre);
        double averageRating_titre = get_averageRating_resultat(resultat, choix);
        printf("Moyenne des évaluations : %.1lf\n", averageRating_titre);
        int numVotes_titres = get_numVotes_resultat(resultat, choix);
        printf("Nombre de votes : %d\n\n", numVotes_titres);
    } else {
        printf("Choix invalide. Le programme va maintenant s'arrêter.\n");
        }
    }

close(socket_client);
printf("\nFermeture de l'application\n\n");
fflush(stdout);
fflush(stdin);
}

/*
 * Programme principal - serveur.c
 *
 * Par : Nicolas Verreault & Briac Samson-tessier
 * Date : 2023-06-22
 *
 * Ce programme permet à l'utilisateur de rechercher des films, jeux vidéo et séries
 * dans une base de données IMDb. L'utilisateur peut spécifier différents critères tels que le titre,
 * la catégorie, l'année ou la période et le genre pour filtrer les résultats.
 * Les titres correspondants aux critères de recherche sont ensuite triés et enregistrés
 * dans un fichier TSV nommé "title_basics.tsv", tandis que les évaluations associées
 * aux titres sont affichées dans le fichier "title_ratings.tsv".
 *
 * Exemple d'exécution:
 *  ./serveur
 */
#include "recherche.h"
#include "communication.h"
#include "resultat.h"
#include "testunitaire.h"
#include "imdb.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "pthread.h"
#include "assert.h"
// Déclaration des mutex
pthread_mutex_t mutex_lecture;
pthread_mutex_t mutex_ecriture;
int nb_reader = 0;
// Déclaration de la fonction de traitement des requêtes client dans un thread séparé
void *process_request(void *params);
// Serveur-HLR01: Découplage du serveur
void main(void)
{
    // Parametres socket
    int socket_serveur;
    int socket_client;
    struct sockaddr_in server_addr;
    struct sockaddr_in client_addr;
    socklen_t server_addr_len;
    socklen_t client_addr_len;
    pthread_t thread;

    // Comm-HLR01: Communication du serveur vers le client
    // *** SECTION INITIALISATION SERVEUR SOCKET *** //

    printf("Serveur à l'écoute sur %d\n", PORT);

    // creation du socket serveur
    socket_serveur = socket(AF_INET, SOCK_STREAM, 0);

    // Activer l'option SO_REUSEADDR
    int enabled = 1;
    int status = setsockopt(socket_serveur, SOL_SOCKET, SO_REUSEADDR, (char *)&enabled, sizeof(int));

    // association IP address et port
    server_addr_len = sizeof(server_addr);
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(PORT);

    // liaison du socket
    bind(socket_serveur, (struct sockaddr *)&server_addr, server_addr_len);
    listen(socket_serveur, 10);

    // Initialisation des mutex
    pthread_mutex_init(&mutex_lecture, NULL);
    pthread_mutex_init(&mutex_ecriture, NULL);

    // Comm-HLR02: Serveur unique et clients multiples
    // Comm-HLR03: Attente de clients
    while (1) {
        // acceptation de la connexion
        socket_client = accept(socket_serveur, (struct sockaddr *)&client_addr, &client_addr_len);
        // Serveur-HLR01: Serveur multitâche multithread
        // lancement de thread
        int *client_socket_ptr = (int *)malloc(sizeof(int));
        *client_socket_ptr = socket_client;
        pthread_create(&thread, NULL, process_request, client_socket_ptr);
    }

    // Serveur-HLR07: Gestion de mémoire allouée par le thread principal
    // Serveur-HLR08: Gestion de mémoire allouée par les threads associés aux clients
    // Fermeture des mutex
    pthread_mutex_destroy(&mutex_lecture);
    pthread_mutex_destroy(&mutex_ecriture);
    // Fermeture du socket
    close(socket_client);
    close(socket_serveur);
}
//fonction qui execute les threads
void *process_request(void *params)
{
    int socket_client = *((int *)params);
    free(params);
    
    // Serveur-HLR02: Maintien de l'intégrité de la base de données en mode lecture
    // Verrouillage en lecture
    pthread_mutex_lock(&mutex_lecture);

    // Serveur-HLR06: Base de données thread-safe
    // Serveur-HLR04: Intégrité des données retournées par les requêtes
    // Gestion des lectures concurrentielles
    nb_reader++;
    if (nb_reader == 1) {
        // Verrouillage en écriture (exclusif) si le premier lecteur
        pthread_mutex_lock(&mutex_ecriture);
    }

    // Libération du verrou de lecture
    pthread_mutex_unlock(&mutex_lecture);

    // Comm-HLR02: Le serveur récupère les critères d'une opération de recherche
    t_critere critere = recuperer_Critere_Recherche(socket_client);
    printf("Exploration de la base de donnee...\n");
    // Serveur-HLR05: Consultation de la base de données en mode multi-tâche
    t_titreCherches resultat = explorer_base_de_donnees(critere);
    explorer_base_de_donnees_cotes(resultat);
    
    // Serveur-HLR06: Base de données thread-safe
    // Serveur-HLR04: Intégrité des données retournées par les requêtes
    // Fin de la lecture, décrémentation du nombre de lecteurs
    nb_reader--;
    if (nb_reader == 0) {
        // Libération du verrou d'écriture si le dernier lecteur
        pthread_mutex_unlock(&mutex_ecriture);
    }

    // Libération du verrou de lecture
    pthread_mutex_unlock(&mutex_lecture);

    // Comm-HLR03: Le serveur retourne les résultats d'une opération de recherche
    printf("Visualisation des resultats\n");
    envoie_resultats_recherche(resultat, socket_client);
    vider_numligne();

    int evaluation = get_evaluation_titre(critere);
    // Serveur-HLR03: Maintien de l'intégrité de la base de données en mode écriture
    // Verrouillage en ecriture
    pthread_mutex_lock(&mutex_ecriture);
    if (evaluation == 1)
    {
        // Comm-HLR07: Le serveur reçoit le titre à évaluer
        int index = reception_index_Evaluation(socket_client);

        // Comm-HLR08: Le serveur envoie la cote de classement du titre choisi
        envoie_cote_Evaluation(resultat, index, socket_client);

        // Comm-HLR11: Le serveur reçoit l'évaluation
        double nouvelle_note = reception_note_Evaluation(socket_client);
        classement_moyen(resultat, index, nouvelle_note);

        // Mise à jour de la base de données
        maj_fichier_cote(resultat, index);

        // Comm-HLR12: Le serveur envoie la nouvelle cote de classement du titre
        envoie_resultats_recherche(resultat, socket_client);
        vider_numligne();
    }
    // Libération du verrou d'écriture
    pthread_mutex_unlock(&mutex_ecriture);

    // Serveur HLR07/HLR08
    detruire_listeTitres(resultat);
    detruire_critere(critere);
    close(socket_client);
    return NULL;
}
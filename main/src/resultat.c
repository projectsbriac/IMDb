/**
 * Programme resultat - resultat.c
 *
 * Par : Nicolas Verreault & Briac Samson-tessier
 * Date : 2023-05-19
 *
 * Ce programme fournit les structures de données encapsulées permettant de
 * conserver les résultats de la recherche effectuée, tels que le titre,
 * l'identifiant unique du film, l'année de parution, le genre, la catégorie,
 * la moyenne de cote et le nombre de votes.
 *
 *  On y retrouve les sous-programmes suivants :
 * - ajout_Cote : Ajoute la cote et le nombre de votes à un titre.
 * - ajouterTitre : Ajoute un titre aux résultats de recherche.
 * - TitreFichier : Écrit les résultats de recherche dans un fichier.
 * - creer_titre : Alloue la mémoire et crée une structure de titre.
 * - creer_listeTitres : Alloue la mémoire et crée une liste de titres recherchés.
 * - detruire_titre : Libère la mémoire d'une structure de titre.
 * - detruire_listeTitres : Libère la mémoire d'une liste de titres recherchés.
 * - set_tconst_resultat : Ajoute manuellement l'identifiant unique du film à une structure de titre.
 * - set_titre_resultat : Ajoute manuellement le titre du film à une structure de titre.
 * - set_genre_resultat : Ajoute manuellement les genres du film à une structure de titre.
 * - set_titleType_resultat : Ajoute manuellement la catégorie du titre à une structure de titre.
 * - set_annee_resultat : Ajoute manuellement l'année de parution à une structure de titre.
 * - set_averageRating_resultat : Ajoute manuellement la moyenne de cote à un titre dans la liste de titres recherchés.
 * - set_numVotes_resultat : Ajoute manuellement le nombre de votes à un titre dans la liste de titres recherchés.
 * - get_tconst_resultat : Récupère l'identifiant unique du film d'un titre dans la liste de titres recherchés.
 * - get_titre_resultat : Récupère le titre du film d'un titre dans la liste de titres recherchés.
 * - get_genre_resultat : Récupère les genres du film d'un titre dans la liste de titres recherchés.
 * - get_titleType_resultat : Récupère la catégorie du titre d'un titre dans la liste de titres recherchés.
 * - get_annee_resultat : Récupère l'année de parution d'un titre dans la liste de titres recherchés.
 * - get_averageRating_resultat : Récupère la moyenne de cote d'un titre dans la liste de titres recherchés.
 * - get_numVotes_resultat : Récupère le nombre de votes d'un titre dans la liste de titres recherchés.
 * - get_nbTitres_resultat : Récupère le nombre de titres recherchés dans la liste de titres recherchés.
 * - get_ligne_resultat : Fonction permettant de recupere le numero de ligne de titres dans la structure de resultat
 **/

#include "resultat.h"

/**
* Structure de données qui permet de contenir toutes les informations sur un titre
**/
// HLR04 : Definition de structure - Titre
struct entreeTitre
{
    char   *tconst;          // Identifiant unique du film
    char   *primaryTitle;    // Titre du film
    int     startYear;       // Année de parution
    char   *genres;          // Genres du film
    char   *titleType;       // Catégorie du titre

    // HLR15 : Modification de structure - Titre
    double averageRating;    // Moyenne de cote sur 10
    int    numVotes;         // Nombre de votes

    //Serveur-HLR03: Modification de la structure titre 
    int     lineNumber;      // Numéro de ligne
};

// HLR06 : Structure de donnees de sauvegarde des titres cherches
struct titreCherches 
{
    struct entreeTitre* titres;    // liste de titres cherches
    int nbTitres;                  // nombres de titres cherches
};

// HLR16 : Fonction publique d'ajout de cote aux titres
void ajout_Cote(t_entree titres, double coteFilm, int nbVotes)
{
    titres->averageRating = coteFilm;
    titres->numVotes = nbVotes;
}

// HLR08 : Fonction ajout titre aux resultatsS
void ajouterTitre(t_titreCherches resultats,t_entree titre) 
{
    // Allouer de la mémoire pour une nouvelle entrée de titre dans les résultats
    struct entreeTitre* nouvelleEntree = (struct entreeTitre*)malloc(sizeof(struct entreeTitre));
    if (nouvelleEntree == NULL) 
    {
        printf("Erreur d'allocation de mémoire.\n");
        return;
    }

    // Copier chaque champ du titre reçu dans la nouvelle entrée
    nouvelleEntree->tconst = strdup(titre->tconst);
    nouvelleEntree->primaryTitle = strdup(titre->primaryTitle);
    nouvelleEntree->startYear = titre->startYear;
    nouvelleEntree->genres = strdup(titre->genres);
    nouvelleEntree->titleType = strdup(titre->titleType);
    nouvelleEntree->averageRating = titre->averageRating;
    nouvelleEntree->numVotes = titre->numVotes;
    nouvelleEntree->lineNumber = titre->lineNumber;
    
    // Ajouter la nouvelle entrée à la liste de titres recherchés
    resultats->titres = (struct entreeTitre*)realloc(resultats->titres, (resultats->nbTitres + 1) * sizeof(struct entreeTitre));
    resultats->titres[resultats->nbTitres] = *nouvelleEntree;
    resultats->nbTitres++;
    detruire_titre(nouvelleEntree);
}

// Serveur HLR02 : Visualisation des resultats
void TitresFichier(t_titreCherches resultats) 
{
    printf("numero\ttconst\ttitleType\tprimaryTitle\tstartYear\tgenres\taverageRating\tnumVotes\n");
    
    for (int i = 0; i < resultats->nbTitres; i++) 
    {
        struct entreeTitre* titre = &(resultats->titres[i]);
        printf("%d\t%s\t%s\t%s\t%d\t%s\t%.1lf\t%d\n",i+1, titre->tconst, titre->titleType, titre->primaryTitle, titre->startYear, titre->genres,titre->averageRating, titre->numVotes);
    }
}

// HLR05 : Allocation de memoires des titres
t_entree creer_titre(void)
{
    // Initialisation de la structure de donnees entreeTitre
    t_entree entreeTitre;

    // Allocation de la memoire sur la structure de donnees entreeTitre
    entreeTitre = malloc(sizeof(struct entreeTitre));

    // Si l'allocation n'est pas NULL, on initie nos variables a NULL
    if (entreeTitre != NULL)
    {
        entreeTitre->tconst = NULL;
        entreeTitre->primaryTitle = NULL;
        entreeTitre->startYear = 0;
        entreeTitre->genres = NULL;
        entreeTitre->titleType = NULL;
    }

    // Retour de la structure entreeTitre
    return entreeTitre;
}

// HLR07 : Allocation de memoire des resultats des titres
t_titreCherches creer_listeTitres(void)
{
    // Initialisation de la structure de donnees titreCherches
    t_titreCherches titreCherches;

    // Allocation de la memoire sur la structure de donnees titreCherches
    titreCherches = (t_titreCherches) malloc(sizeof(struct titreCherches));

    // Si l'allocation n'est pas NULL, on initie nos variables a NULL
    if (titreCherches != NULL)
    {
        titreCherches->titres = NULL;
        titreCherches->nbTitres = 0;
    }

    // Retour de notre structure titreCherches
    return titreCherches;
}

//Serveur-HLR05: Ajout d'une note au classement moyen d'un titre
void classement_moyen(t_titreCherches resultat,int index ,double nouvelle_note) {
    // Récupérer la note moyenne et le nombre de votes actuels du titre
    if(resultat->titres[index].averageRating!=0){
    double ancienne_moyenne = resultat->titres[index].averageRating;
    int nombre_votes = resultat->titres[index].numVotes;

    // Calculer la nouvelle moyenne en utilisant la formule de pondération des votes
    double nouvelle_moyenne = (ancienne_moyenne * nombre_votes + nouvelle_note) / (nombre_votes + 1);
    // Mettre à jour la nouvelle moyenne dans la structure du titre
    resultat->titres[index].averageRating=nouvelle_moyenne;
    }
    //Serveur-HLR06: Ajout d'une note à un titre sans classement
    else{
        resultat->titres[index].averageRating=nouvelle_note;
    }
    //incrementation nombre de vote
    resultat->titres[index].numVotes++;
}

// Destruction entreeTitre
void detruire_titre(t_entree entreeTitre)
{
    // Libere la memoire de entreeTitre
    free(entreeTitre);
}

// Destructeur liste de titres cherches
void detruire_listeTitres(t_titreCherches titreCherches)
{
    // Libere la memoire de titreCherches
    free(titreCherches);
}

//fonction permettant d'ajouter manuellement tconst dans la structure de titre
void set_tconst_resultat(t_entree titres,char *tconst)
{
    titres->tconst = tconst;
}

//fonction permettant d'ajouter manuellement le titre dans la structure de titre
void set_titre_resultat(t_entree titres,char *primaryTitle)
{
    titres->primaryTitle = primaryTitle;
}

//fonction permettant d'ajouter manuellement le genre dans la structure de titre
void set_genre_resultat(t_entree titres,char *genres)
{
    titres->genres = genres;
}

//fonction permettant d'ajouter manuellement le type du film dans la structure de titre
void set_titleType_resultat(t_entree titres, char *titleType)
{
    titres->titleType = titleType;
}

//fonction permettant d'ajouter l'anne  dans la structure de titre
void set_annee_resultat(t_entree titres, int startYear)
{
    titres->startYear = startYear;
}
// Fonction permettant d'ajouter la moyenne de cote dans la structure de titre
void set_averageRating_titre(t_entree titre, double averageRating)
{
    titre->averageRating = averageRating;
}

// Fonction permettant d'ajouter la moyenne de cote dans la structure de titre
void set_numVotes_titre(t_entree titre, int numVotes)
{
    titre->numVotes = numVotes;
}

void set_ligne_titre(t_entree titre,int lineNumber) 
{
    titre->lineNumber= lineNumber;
}
// Fonction permettant d'ajouter la moyenne de cote dans la structure de titre
void set_averageRating_resultat(t_titreCherches resultat, double averageRating,int index)
{
    resultat->titres[index].averageRating = averageRating;
}

// Fonction permettant d'ajouter le nombre de votes dans la structure de titre
void set_numVotes_resultat(t_titreCherches resultat, int numVotes,int index)
{
    resultat->titres[index].numVotes = numVotes;
}

// Fonction permettant d'ajouter la ligne dans la structure de titre
void set_ligne_resultat(t_titreCherches resultat,int numLigne,int index)
{
    resultat->titres[index].lineNumber = numLigne;
}

// Fonction permettant de recupere le tconst dans la structure de titre desire par l'index
char* get_tconst_resultat(t_titreCherches resultat,int index) 
{
    return resultat->titres[index].tconst;
}

// Fonction permettant de recupere le titre dans la structure de titre desire par l'index
char* get_titre_resultat(t_titreCherches resultat,int index) 
{
    return resultat->titres[index].primaryTitle;
}

// Fonction permettant de recupere le genre dans la structure de titre desire par l'index
char* get_genre_resultat(t_titreCherches resultat,int index) 
{
    return resultat->titres[index].genres;
}

// Fonction permettant de recupere la categorie dans la structure de titre desire par l'index
char* get_titleType_resultat(t_titreCherches resultat,int index) 
{
    return resultat->titres[index].titleType;
}

// Fonction permettant de recupere l'annee dans la structure de titre desire par l'index
int get_annee_resultat(t_titreCherches resultat,int index) 
{
    return resultat->titres[index].startYear;
}

// Fonction permettant de recupere la note moyenne dans la structure de titre desire par l'index
double get_averageRating_resultat(t_titreCherches resultat,int index) 
{
    return resultat->titres[index].averageRating;
}

// Fonction permettant de recupere le nombre de votes dans la structure de titre desire par l'index
int get_numVotes_resultat(t_titreCherches resultat,int index) 
{
    return resultat->titres[index].numVotes;
}

// Fonction permettant de recupere le nombre de titres dans la structure de resultat
int get_nbTitres_resultat(t_titreCherches resultat) 
{
    return resultat->nbTitres;
}
// Fonction permettant de recupere le numero de ligne de titres dans la structure de resultat
int get_ligne_resultat(t_titreCherches resultat,int index) 
{
    return resultat->titres[index].lineNumber;
}


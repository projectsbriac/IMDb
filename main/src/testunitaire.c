/**
 * Module de tests - testunitaire.c
 *
 * Par : Nicolas Verreault & Briac Samson-tessier
 * Date : 2023-06-08
 *
 * Ce module sert a tester le bon fonctionnement du programme du laboratoire 2. Plus specifiquement, 
 * il sert a s'assurer du bon fonctionnement des modules recherche.c et recherche.h.
 */

#include "testunitaire.h"



// Fonction de test pour la création et la destruction du critère
void test_creer_detruire_critere(void) 
{
    t_critere critere = creer_critere();

    printf("\n*** TEST DE LA FONCTION CREER_CRITERE() ***\n");

    assert(critere != NULL);

    printf("\nLe test est un succes.\n");

    detruire_critere(critere);
}

// Fonction de test pour les mutateurs et les observateurs du critère
void test_mutateurs_observateurs_critere(void) 
{
    t_critere critere = creer_critere();

    printf("\n*** TEST DE LA FONCTION  SET_TITRE() ***\n");

     // Test valide : Titre = titre
    set_titre(critere, "Titre");
    assert(strcmp(get_titre(critere), "Titre") == 0);
    printf("\nLe test est un succes.\n");

    printf("\n*** TEST DE LA FONCTION  SET_GENRE() ***\n");

    // Test valide : genre = genre
    set_genre(critere, "Genre");
    assert(strcmp(get_genre(critere), "Genre") == 0);
    printf("\nLe test est un succes.\n");

    printf("\n*** TEST DE LA FONCTION  SET_CATEGORIE() ***\n");

    // Test valide : categorie = categorie
    set_categorie(critere, "Categorie");
    assert(strcmp(get_categorie(critere), "Categorie") == 0);
    printf("\nLe test est un succes.\n");

    printf("\n*** TEST DE LA FONCTION  SET_ANNEE_PARUTION_MIN() ***\n");

    // Test valide : annee min = 2000
    set_annee_parution_min(critere, 2000);
    assert(get_annee_parution_min(critere) == 2000);
    printf("\nLe test est un succes.\n");

    printf("\n*** TEST DE LA FONCTION  SET_ANNEE_PARUTION_MAX() ***\n");

    // Test valide : annee max = 2020
    set_annee_parution_max(critere, 2020);
    assert(get_annee_parution_max(critere) == 2020);
    printf("\nLe test est un succes.\n");

    // Test invalide : Titre NULL
    printf("\n*** TEST INVALIDE : Titre NULL ***\n");
    set_titre(critere, NULL);
    assert(get_titre(critere) == NULL);
    printf("Le test est un succès.\n");

    // Test invalide : Genre NULL
    printf("\n*** TEST INVALIDE : Genre NULL ***\n");
    set_genre(critere, NULL);
    assert(get_genre(critere) == NULL);
    printf("Le test est un succès.\n");

    // Test invalide : Catégorie NULL
    printf("\n*** TEST INVALIDE : Catégorie NULL ***\n");
    set_categorie(critere, NULL);
    assert(get_categorie(critere) == NULL);
    printf("Le test est un succès.\n");

    // Test invalide : Année de parution minimum supérieure à l'année de parution maximum
    printf("\n*** TEST INVALIDE : Année de parution minimum supérieure à l'année de parution maximum ***\n");
    set_annee_parution_min(critere, 2022);
    set_annee_parution_max(critere, 2020);
    assert(get_annee_parution_min(critere) == 2022);
    assert(get_annee_parution_max(critere) == 2020);
    printf("Le test est un succès.\n");

    detruire_critere(critere);
}

// Fonction de test pour la fonction set_intervalle_annees()
void test_set_intervalle_annees(void)
 {
    t_critere critere = creer_critere();
    printf("\n*** TEST DE LA FONCTION  set_intervalle_annees() ***\n");
    
    // Test valide : annee = 2000
    set_intervalle_annees(critere, "2000");
    assert(get_annee_parution_min(critere) == 2000);
    assert(get_annee_parution_max(critere) == 2000);
    printf("Le test est un succes.\n");
/*
    set_intervalle_annees(critere,"2000:2029");
    assert(get_annee_parution_min(critere) == 2000);
    assert(get_annee_parution_max(critere) == 2029);
    printf("Le test est un succes.\n");
    detruire_critere(critere);
*/
}

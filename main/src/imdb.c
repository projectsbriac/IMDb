/**
 * Programme imdb - imdb.c
 *
 * Par : Nicolas Verreault & Briac Samson-tessier
 * Date : Juin 2023
 *
 * Ce module contient les fonctions pour l'exploration de la base de données de titres et de cotes
 * dans des fichiers situe dans le repertoire parents (title_basics.tsv et title_ratings.tsv)
 * ce module va comparer des critères de recherche avec les lignes du fichier title_basics.tsv
 * et ajouter les titres dans la structure resultat du module resultat.c.
 * imdb.c contient egalement une fonction pour comparer les tcsont avec les lignes du fichier title_ratings.tsv
 * pour ainsi, pouvoir ajouter les cotes au tconst correspond au titres.
 * 
 * Fonctions disponibles :
 * - decomposer_ligne : Décompose une ligne du fichier title_basics.tsv en champs distincts.
 * - decomposer_ligne_cote : Décompose une ligne de cotes du fichier title_ratings.tsv en champs distincts.
 * - comparer_titre : Compare un titre avec les critères de recherche.
 * - comparer_cote : Compare un titre avec les cotes retenues.
 * - explorer_base_de_donnees : Exploration de la base de données de titres.
 * - explorer_base_de_donnees_cotes : Exploration de la base de données des cotes.
 **/

#include "imdb.h"
#include "recherche.h"
#include "resultat.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

// Définir un verrou global pour protéger l'accès à fgets
pthread_mutex_t fgets_mutex = PTHREAD_MUTEX_INITIALIZER;
// Fonction prototype
int comparer_titre(t_critere critere);
int comparer_cote(t_titreCherches resultats);
void decomposer_ligne(char *ligne);
void decomposer_ligne_cote( char *ligneCote);

// Définition de variables globales similaires à la structure de titres
char* tconst;
char* tconst_cote;
char* primaryTitle;
char* genres;
int startYear;
char* titleType;
double averageRating;
int numVotes =0;
int numLigne = 0;

// HLR12 : Fonction privée pour décomposer les champs d'une ligne du fichier title_basics.tsv
 void decomposer_ligne( char *ligne) {
    tconst = strsep(&ligne, "\t");
    titleType = strsep(&ligne, "\t");
    primaryTitle = strsep(&ligne, "\t");
    strsep(&ligne, "\t"); 
    strsep(&ligne, "\t");
    char* startYear_str = strsep(&ligne, "\t");
    strsep(&ligne, "\t"); 
    strsep(&ligne, "\t"); 
    genres = strsep(&ligne, "\t");

    //on utilise la fonction atoi pour que startYear soit un entier et non une chaine de caractere
    if (startYear_str != NULL) {
        startYear = atoi(startYear_str);
    }
}

// HLR20 : Fonction privee pour decomposer les champs d'une ligne de cotes du fichier title_ratings.tsv
void decomposer_ligne_cote( char *ligneCote) 
{

    tconst_cote = strsep(&ligneCote, "\t");
    char* averageRating_str = strsep(&ligneCote, "\t");
    char* numVotes_str = strsep(&ligneCote, "\t");

    //on utilise la fonction atof pour que averageRating soit un double et non une chaine de caractere
    if (averageRating_str != NULL) {
        averageRating = atof(averageRating_str);
    }
    //on utilise la fonction atoi pour que numVotes soit un entier et non une chaine de caractere
    if (numVotes_str != NULL) {
        numVotes = atoi(numVotes_str);
    }

}

// HLR10 : Exploration de la base de donnees de titres 
t_titreCherches explorer_base_de_donnees(t_critere critere) {
    // Ouvrir le fichier title_basics.tsv en mode lecture
    FILE* file = fopen("../title_basics.tsv", "r");
    if (file == NULL) {
        printf("Erreur : Impossible d'ouvrir le fichier title_basics.tsv.\n");
        return NULL;
    }
    // Créer une structure de résultats
    t_titreCherches resultats = creer_listeTitres();
    // HLR11 : Ouverture et parcours des titres
    char line[1024];
    // Verrouiller le verrou avant d'accéder au fichier avec fgets
    pthread_mutex_lock(&fgets_mutex);
    LOG("En recherche ...\n");
    // Boucle while qui lit ligne par ligne
    while (fgets(line, sizeof(line), file)) 
    {
        // Décomposer la ligne en utilisant la fonction decomposer_ligne
        decomposer_ligne(line);
        // Vérifier si le titre correspond aux critères de recherche
        if (comparer_titre(critere)) {
            // HLR14 : ajout des titres a la strucutre de titre
            // Créer une nouvelle structure t_entree et copier les données
            t_entree titre = creer_titre();
            set_tconst_resultat(titre, tconst);
            set_titre_resultat(titre, primaryTitle);
            set_genre_resultat(titre, genres);
            set_titleType_resultat(titre, titleType);
            set_annee_resultat(titre, startYear);

            // Ajouter le titre aux résultats
            ajouterTitre(resultats, titre);
            detruire_titre(titre);
        }
    }
    // Déverrouiller le verrou après avoir terminé l'accès au fichier avec fgets
    LOG("Recherche terminee!\n");
    pthread_mutex_unlock(&fgets_mutex);
    // Fermer le fichier
    fclose(file);
    return resultats;
}

// HLR18 : Exploration de la base de donnees des cotes
void explorer_base_de_donnees_cotes(t_titreCherches resultats)
{
    pthread_mutex_lock(&fgets_mutex);
    // Ouvrir le fichier title_basics.tsv en mode lecture
    FILE* fileRatings = fopen("../title_ratings.tsv", "r");
    
    if (fileRatings == NULL) 
    {
        printf("Erreur : Impossible d'ouvrir le fichier title_ratings.tsv.\n");
    }

    // HLR19 : Ouverture et parcours des cotes
    char ligneCote[1024];
    // Verrouiller le verrou avant d'accéder au fichier avec fgets
    
    // Boucle while qui lit ligne par ligne
    while (fgets(ligneCote, sizeof(ligneCote), fileRatings)) 
    {
        //Serveur-HLR04: Emmagasinage du numéro de ligne d'un classement 
        numLigne++;
        //Décomposer la ligne en utilisant la fonction decomposer_ligne_cote
        decomposer_ligne_cote(ligneCote);
        comparer_cote(resultats);
    }
    // Déverrouiller le verrou après avoir terminé l'accès au fichier avec fgets
    pthread_mutex_unlock(&fgets_mutex);
    fclose(fileRatings);
}

// HLR13 : Fonction privée pour comparer un titre avec les critères de recherche
int comparer_titre(t_critere critere) {
    //on recupere les donnees de la structure titre
    char* critere_titre=get_titre(critere);
    char* critere_genre=get_genre(critere);
    char* critere_categorie=get_categorie(critere);
    int critere_annee_parution_max=get_annee_parution_max(critere);
    int critere_annee_parution_min=get_annee_parution_min(critere);


    // Comparer le titre donné avec le champ primaryTitle du titre décomposé
    int titre_bon = 0;
    int longueurprimaryTitle = strlen(primaryTitle);
    int longueurcritere_titre = strlen(critere_titre);

    // Parcourir chaque caractère de la phrase pour le titre
    for (int i = 0; i <= longueurprimaryTitle - longueurcritere_titre; i++) {
        int j;
        // Vérifier si le caractère courant correspond au premier caractère du mot
        if (primaryTitle[i] == critere_titre[0]) {
            // Vérifier si les caractères suivants correspondent au reste du mot
            for (j = 1; j < longueurcritere_titre; j++) {
                if (primaryTitle[i + j] != critere_titre[j]) {
                    break;
                }
            }
            // Si tous les caractères correspondent, le mot a été trouvé
            if (j == longueurcritere_titre) {
                titre_bon = 1;
            }
        }
    }


    //verification de la categorie
    if (critere_categorie != NULL && strcmp(critere_categorie, titleType) != 0) {
        return 0; // Les catégories de titre ne concordent pas, retourner 0
    }
    //verification de l'annee
    if (critere_annee_parution_max != -1 && startYear > critere_annee_parution_max) {
        return 0; // L'anne de titre est superieur a celle du critere, retourner 0
    }
    if (critere_annee_parution_min != -1 && startYear < critere_annee_parution_min) {
        return 0; // L'anne de titre est inferieur a celle du critere, retourner 0
    }

    // Comparer le genre donné avec le champ genres du titre décomposé
    int genre_bon = 0;
    if (critere_genre != NULL) {
        int longueurgenres = strlen(genres);
        int longueurcritere_genres = strlen(critere_genre);

        // Parcourir chaque caractère de la phrase pour le titre
        for (int i = 0; i <= longueurgenres - longueurcritere_genres; i++) {
            int j;
            // Vérifier si le caractère courant correspond au premier caractère du mot
            if (genres[i] == critere_genre[0]) {
                // Vérifier si les caractères suivants correspondent au reste du mot
                for (j = 1; j < longueurcritere_genres; j++) {
                    if (genres[i + j] != critere_genre[j]) {
                        break;
                    }   
                }
                // Si tous les caractères correspondent, le mot a été trouvé
                if (j == longueurcritere_genres) {
                genre_bon = 1;
                }
            }
        }
    }
    else{
        genre_bon = 1;//pas d'entree de genre ainsi genre_bon=1 
    }
    // genre_bon est toujours egale a 1 si on ne specifie pas de genre dans les criteres
    if (titre_bon == 1 && genre_bon == 1) { 
        //on return 1 pour signifier ques les titres correspondent
        return 1;
    } 
    //le titre ne correpond pas aux criterex
    else {
        return 0;
    }
}


// HLR21: Fonction privée pour comparer un titre avec les cotes retenues
int comparer_cote(t_titreCherches resultats){
    int nbTitre_cote = get_nbTitres_resultat(resultats);
    //on parcourt chaque titres contenue dans la structure resultats
    for(int index=0;index<nbTitre_cote;index++){    
        //on recupere le tconst correspondant au titre[index]
        char* resultats_tconst=get_tconst_resultat(resultats,index);
        //verification de tconst
        if (strcmp(resultats_tconst, tconst) == 0) {
            //Serveur-HLR04: Emmagasinage du numéro de ligne d'un classement 
            set_ligne_resultat(resultats,numLigne,index);
            //on ajoute le nombres de votes et note moyenne a la structure de titres
            //correspondant a l'index
            set_numVotes_resultat(resultats, numVotes,index);
            set_averageRating_resultat(resultats, averageRating,index);

        }
  
    }
    return 1;
}

// Labo3 Serveur-HLR07: Génération d'un nouveau fichier de cotes de classement
void maj_fichier_cote(t_titreCherches resultat,int index)
{
    // Ouvrir le fichier title_basics.tsv en mode lecture
    FILE* fileRatings = fopen("../title_ratings.tsv", "r");

    // Ouvrir le fichier title_basics_temp.tsv en mode ecriture
    FILE* fileRatingsTemp = fopen("../title_ratings_temp.tsv", "w");
    

    if (fileRatings == NULL) 
    {
        printf("Erreur : Impossible d'ouvrir le fichier title_ratings.tsv dans maj_fichier_cote.\n");
    }

    if (fileRatingsTemp == NULL) 
    {
        printf("Erreur : Impossible d'ouvrir le fichier title_ratings_temp.tsv dans maj_fichier_cote.\n");
    }
    char* nouveau_tconst=get_tconst_resultat(resultat,index);
    double nouvelle_cote=get_averageRating_resultat(resultat,index);
    int nouveau_Numvote=get_numVotes_resultat(resultat,index);
    
    // Parcourir les lignes du fichier title_ratings.tsv
    char ligneCote[1024];
    int numeroLigne = 0;
    int titreTrouve = 0;
    int id_ligne =get_ligne_resultat(resultat,index);
    bool premiereLigneEcrit = false;

    while (fgets(ligneCote, sizeof(ligneCote), fileRatings))
    {
        numeroLigne++;

        if (!premiereLigneEcrit && numeroLigne == id_ligne)
        {
            fprintf(fileRatingsTemp, "%s\t%.1lf\t%d\n", nouveau_tconst, nouvelle_cote, nouveau_Numvote);
            premiereLigneEcrit = true;
        }
        else
        {
        fprintf(fileRatingsTemp, "%s", ligneCote);
        }
    }

    // Serveur-HLR10: Ajout d'une nouvelle entree de cote de classement 
    // Vérifier si le titre est déjà présent dans le fichier
   
    if (id_ligne == 0 ){
    fprintf(fileRatingsTemp, "%s\t%.1lf\t%d\n", nouveau_tconst, nouvelle_cote,nouveau_Numvote);
    }

    // Fermer les fichiers
    fclose(fileRatings);
    fclose(fileRatingsTemp);

    // Supprimer l'ancien fichier title_ratings.tsv
    remove("../title_ratings.tsv");

    // Renommer le fichier temporaire en title_ratings.tsv
    rename("../title_ratings_temp.tsv", "../title_ratings.tsv");
}

void vider_numligne(void)
{
    numLigne=0;
}
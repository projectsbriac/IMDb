/**
 * Programme communication - communication.c
 *
 * Par : Nicolas Verreault & Briac Samson-tessier
 * Date : Juillet 2023
 *
 * Ce module est responsable des fonctions pour la communication entre les fichier client.c
 * et le serveur.c. Toutes les fonctions presentes servent a ouvrir un socket, communiquer 
 * l'information et fermer le socket.
 * 
 * Fonctions disponibles :
 * - free_Communication() : Libere les criteres utilises pour chercher un titre
 * - recuperer_Critere_Recherche() : Recupere les criteres de recherches par socket
 * - envoyer_Critere_Recherche() : Envoie les criteres de recherches par socket
 * - envoie_resultats_recherche() : Envoie des donnees de resultats de recherches par socket
 * - t_titreCherches reception_resultats_recherche() : Recoit les donnees des resultats de recherches par socket
 * - envoie_index_Evaluation() : Envoie une variable index par socket
 * - reception_index_Evaluation() : Recoit la variable index par socket
 * - envoie_cote_Evaluation() : Envoie la cote d'evaluation par socket
 * - reception_cote_Evaluation() : Recoit la cote d'evaluation par socket
 * - envoie_note_Evaluation() : Envoie la note d'evaluation par socket
 * - reception_note_Evaluation() : Recoit la note d'evaluation par socket
 **/

#include "communication.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

char id_champ[FIELD_ID_SIZE];

// Comm-HLR01: Le client fait une opération de recherche
void envoyer_Critere_Recherche(t_critere critere, int socket_id) 
{
    // Envoi du titre
    char* titre = get_titre(critere);
    if (titre != NULL) {
        int taille_titre = strlen(titre) + 1;
        char* transfert_titre = malloc(taille_titre * sizeof(char));
        strcpy(transfert_titre, titre);
        strcpy(id_champ, "Titre");
        write(socket_id, id_champ, FIELD_ID_SIZE * sizeof(char));
        write(socket_id, &taille_titre, sizeof(int));
        write(socket_id, transfert_titre, taille_titre * sizeof(char));
        free(transfert_titre);
    }

    // Envoi du genre
    char* genre = get_genre(critere);
    if (genre != NULL) {
        int taille_genre = strlen(genre) + 1;
        char* transfert_genre = malloc(taille_genre * sizeof(char));
        strcpy(transfert_genre, genre);
        strcpy(id_champ, "Genre");
        write(socket_id, id_champ, FIELD_ID_SIZE * sizeof(char));
        write(socket_id, &taille_genre, sizeof(int));
        write(socket_id, transfert_genre, taille_genre * sizeof(char));
        free(transfert_genre);
    }

    // Envoi de la catégorie
    char* categorie = get_categorie(critere);
    if (categorie != NULL) {
        int taille_categorie = strlen(categorie) + 1;
        char* transfert_categorie = malloc(taille_categorie * sizeof(char));
        strcpy(transfert_categorie, categorie);
        strcpy(id_champ, "Categorie");
        write(socket_id, id_champ, FIELD_ID_SIZE * sizeof(char));
        write(socket_id, &taille_categorie, sizeof(int));
        write(socket_id, transfert_categorie, taille_categorie * sizeof(char));
        free(transfert_categorie);
    }

    // Envoi de l'année de parution minimale
    int annee_min = get_annee_parution_min(critere);
    strcpy(id_champ, "AnneeMin");
    write(socket_id, id_champ, FIELD_ID_SIZE * sizeof(char));
    write(socket_id, &annee_min, sizeof(int));

    // Envoi de l'année de parution maximale
    int annee_max = get_annee_parution_max(critere);
    strcpy(id_champ, "AnneeMax");
    write(socket_id, id_champ, FIELD_ID_SIZE * sizeof(char));
    write(socket_id, &annee_max, sizeof(int));

    // Envoi de l'évaluation du titre
    int evaluation_titre = get_evaluation_titre(critere);
    strcpy(id_champ, "EvalTitre");
    write(socket_id, id_champ, FIELD_ID_SIZE * sizeof(char));
    write(socket_id, &evaluation_titre, sizeof(int));

    strcpy(id_champ, "FIN");
    write(socket_id, id_champ, FIELD_ID_SIZE*sizeof(char));
}


// Comm-HLR02: Le serveur récupère les critères d'une opération de recherche
t_critere recuperer_Critere_Recherche(int socket_id) {
    int fini = 0; // Variable de confirmation de terminaison d'une fonction
    int taille_titre, taille_genre, taille_categorie; // Variables des tailles des char*
    char* Titre = NULL; // Variable titre
    char* Genres = NULL; // Variable genre
    char* Categorie = NULL; // Variable categorie
    int annee_parution_min = 0; // variable annee_parution_min
    int annee_parution_max = 0; // variable annee_parution_max
    int evaluationTitre = 0; // variable evaluationTitre

    while (!fini) {
        // Lecture du id_chap
        read(socket_id, id_champ, FIELD_ID_SIZE * sizeof(char));
        if (!strcmp(id_champ, "FIN")) {
            fini = 1;
        } else if (!strcmp(id_champ, "Titre")) {
            // Lecture de la taille de titre
            read(socket_id, &taille_titre, sizeof(int));
            Titre = malloc(taille_titre * sizeof(char));
            // Lecture du titre
            read(socket_id, Titre, taille_titre * sizeof(char));
            printf("Titre: %s\n", Titre);
        } else if (!strcmp(id_champ, "Genre")) {
            // Lecture de la taille de genre
            read(socket_id, &taille_genre, sizeof(int));
            Genres = malloc(taille_genre * sizeof(char));
            // Lecture de genre
            read(socket_id, Genres, taille_genre * sizeof(char));
            printf("Genre: %s\n", Genres);
        } else if (!strcmp(id_champ, "Categorie")) {
            // Lecture de la taille de la categorie
            read(socket_id, &taille_categorie, sizeof(int));
            Categorie = malloc(taille_categorie * sizeof(char));
            // Lecture de la categorie
            read(socket_id, Categorie, taille_categorie * sizeof(char));
            printf("Categorie: %s\n", Categorie);
        } else if (!strcmp(id_champ, "AnneeMin")) {
            // Lecture de l'annee de parution minimum
            read(socket_id, &annee_parution_min, sizeof(int));
        } else if (!strcmp(id_champ, "AnneeMax")) {
            // Lecture de l'annee de parution maximal
            read(socket_id, &annee_parution_max, sizeof(int));
        } else if (!strcmp(id_champ, "EvalTitre")) {
            // Lecture de l'evaluation de titre
            read(socket_id, &evaluationTitre, sizeof(int));
        } else {
            printf("Erreur: identifiant de champ inconnu\n");
            exit(1);
        }
    }
    printf("\n");

t_critere critere = creer_critere();
set_titre(critere, Titre);
if (Categorie) 
    set_categorie(critere, Categorie);
if (Genres) 
    set_genre(critere, Genres);
if (annee_parution_min) 
    set_annee_parution_min(critere, annee_parution_min);
if (annee_parution_max) 
    set_annee_parution_max(critere, annee_parution_max);    
if (evaluationTitre) 
    set_evaluation_titre(critere, evaluationTitre);
return critere;
}

// Comm-HLR03: Le serveur retourne les résultats de titres d'une recherche vers le client.
void envoie_resultats_recherche(t_titreCherches resultat, int socket_id)
{
    // Envoi du nombre de résultats de recherche au client
    int transfert_nbTitres = get_nbTitres_resultat(resultat);
    int noctets = write(socket_id, &transfert_nbTitres, sizeof(int));
    if (noctets < sizeof(int)) {
        printf("Problème lors de l'écriture\n");
        exit(1);
    }

    // Envoi des résultats de recherche au client
    for (int i = 0; i < transfert_nbTitres; i++) {

        // Envoi de la taille de primaryTitle
        char* resultat_titre=get_titre_resultat(resultat,i);
        int taille_primaryTitle = strlen(resultat_titre) + 1;
        noctets = write(socket_id, &taille_primaryTitle, sizeof(int));
        if (noctets < sizeof(int)) {
            printf("Problème lors de l'écriture\n");
            exit(1);
        }

        // Envoi du resultat_titre
        noctets = write(socket_id, resultat_titre, taille_primaryTitle * sizeof(char));
        if (noctets < taille_primaryTitle * sizeof(char)) {
            printf("Problème lors de l'écriture\n");
            exit(1);
        }

        // Envoi de la taille de tconst
        char* resultat_tconst=get_tconst_resultat(resultat,i);
        int taille_tconst = strlen(resultat_tconst) + 1;
        noctets = write(socket_id, &taille_tconst, sizeof(int));
        if (noctets < sizeof(int)) {
            printf("Problème lors de l'écriture\n");
            exit(1);
        }

        // Envoi du resultat_tconst
        noctets = write(socket_id, resultat_tconst, taille_tconst * sizeof(char));
        if (noctets < taille_tconst * sizeof(char)) {
            printf("Problème lors de l'écriture\n");
            exit(1);
        }

        // Envoi de startYear
        int resultat_annees=get_annee_resultat(resultat,i);
        noctets = write(socket_id, &(resultat_annees), sizeof(int));
        if (noctets < sizeof(int)) {
            printf("Problème lors de l'écriture\n");
            exit(1);
        }

        // Envoi de la taille de genres
        char* resultat_genre=get_genre_resultat(resultat,i);
        int taille_genres = strlen(resultat_genre) + 1;
        noctets = write(socket_id, &taille_genres, sizeof(int));
        if (noctets < sizeof(int)) {
            printf("Problème lors de l'écriture\n");
            exit(1);
        }

        // Envoie du resultat_genre
        noctets = write(socket_id, resultat_genre, taille_genres * sizeof(char));
        if (noctets < taille_genres * sizeof(char)) {
            printf("Problème lors de l'écriture\n");
            exit(1);
        }

        // Envoi de la taille de titleType
        char* resultat_titleType=get_titleType_resultat(resultat,i);
        int taille_titleType = strlen(resultat_titleType) + 1;
        noctets = write(socket_id, &taille_titleType, sizeof(int));
        if (noctets < sizeof(int)) {
            printf("Problème lors de l'écriture\n");
            exit(1);
        }

        // Envoie de resultat_titleType
        noctets = write(socket_id, resultat_titleType, taille_titleType * sizeof(char));
        if (noctets < taille_titleType * sizeof(char)) {
            printf("Problème lors de l'écriture\n");
            exit(1);
        }

        // Envoi de averageRating
        double resultat_averageRating=get_averageRating_resultat(resultat,i);
        noctets = write(socket_id, &(resultat_averageRating), sizeof(double));
        if (noctets < sizeof(double)) {
            printf("Problème lors de l'écriture\n");
            exit(1);
        }

        // Envoi de numVotes
        int resultat_numVotes=get_numVotes_resultat(resultat,i);
        noctets = write(socket_id, &(resultat_numVotes), sizeof(int));
        if (noctets < sizeof(int)) {
            printf("Problème lors de l'écriture\n");
            exit(1);
        }

        // Envoi de lineNumber
        int resultat_lineNumber=get_ligne_resultat(resultat,i);
        noctets = write(socket_id, &(resultat_lineNumber), sizeof(int));
        if (noctets < sizeof(int)) {
            printf("Problème lors de l'écriture\n");
            exit(1);
        }
    }
}

// Comm-HLR04: Le client recoit les résultats de titres d'une recherche depuis le serveur.
t_titreCherches reception_resultats_recherche(int socket_id)
{
    t_titreCherches resultats = creer_listeTitres();

    // Lecture du nombre de résultats de recherche
    int transfert_nbTitres;
    int noctets = read(socket_id, &transfert_nbTitres, sizeof(int));
    if (noctets < sizeof(int)) {
        printf("Problème lors de la lecture\n");
        exit(1);
    }

    // Lecture des résultats de recherche
    for (int i = 0; i < transfert_nbTitres; i++) {

        // Lecture de la taille de primaryTitle
        int taille_primaryTitle;
        noctets = read(socket_id, &taille_primaryTitle, sizeof(int));
        if (noctets < sizeof(int)) {
            printf("Problème lors de la lecture\n");
            exit(1);
        }

        // Lecture de primaryTitle
        char* transfert_primaryTitle = (char*)malloc(taille_primaryTitle * sizeof(char));
        noctets = read(socket_id, transfert_primaryTitle, taille_primaryTitle * sizeof(char));
        if (noctets < taille_primaryTitle * sizeof(char)) {
            printf("Problème lors de la lecture\n");
            exit(1);
        }

        // Lecture de la taille de tconst
        int taille_tconst;
        noctets = read(socket_id, &taille_tconst, sizeof(int));
        if (noctets < sizeof(int)) {
            printf("Problème lors de la lecture\n");
            exit(1);
        }

        // Lecture de tconst
        char* transfert_tconst = (char*)malloc(taille_tconst * sizeof(char));
        noctets = read(socket_id, transfert_tconst, taille_tconst * sizeof(char));
        if (noctets < taille_tconst * sizeof(char)) {
            printf("Problème lors de la lecture\n");
            exit(1);
        }

        // Lecture de startYear
        int transfert_startYear;
        noctets = read(socket_id, &transfert_startYear, sizeof(int));
        if (noctets < sizeof(int)) {
            printf("Problème lors de la lecture\n");
            exit(1);
        }

        // Lecture de la taille de genres
        int taille_genres;
        noctets = read(socket_id, &taille_genres, sizeof(int));
        if (noctets < sizeof(int)) {
            printf("Problème lors de la lecture\n");
            exit(1);
        }

        // Lecture de genres
        char* transfert_genres = (char*)malloc(taille_genres * sizeof(char));
        noctets = read(socket_id, transfert_genres, taille_genres * sizeof(char));
        if (noctets < taille_genres * sizeof(char)) {
            printf("Problème lors de la lecture\n");
            exit(1);
        }

        // Lecture de la taille de titleType
        int taille_titleType;
        noctets = read(socket_id, &taille_titleType, sizeof(int));
        if (noctets < sizeof(int)) {
            printf("Problème lors de la lecture\n");
            exit(1);
        }

        // Lecture de titleTpe
        char* transfert_titleType = (char*)malloc(taille_titleType * sizeof(char));
        noctets = read(socket_id, transfert_titleType, taille_titleType * sizeof(char));
        if (noctets < taille_titleType * sizeof(char)) {
            printf("Problème lors de la lecture\n");
            exit(1);
        }

        // Lecture de averageRating
        double transfert_averageRating;
        noctets = read(socket_id, &transfert_averageRating, sizeof(double));
        if (noctets < sizeof(double)) {
            printf("Problème lors de la lecture\n");
            exit(1);
        }

        // Lecture de numVotes
        int transfert_numVotes;
        noctets = read(socket_id, &transfert_numVotes, sizeof(int));
        if (noctets < sizeof(int)) {
            printf("Problème lors de la lecture\n");
            exit(1);
        }

        // Lecture de lineNumber
        int transfert_lineNumber;
        noctets = read(socket_id, &transfert_lineNumber, sizeof(int));
        if (noctets < sizeof(int)) {
            printf("Problème lors de la lecture\n");
            exit(1);
        }

        // Remplissage de la structure entreeTitre vers client
        t_entree titre = creer_titre();
        set_tconst_resultat(titre, transfert_tconst);
        set_titre_resultat(titre, transfert_primaryTitle);
        set_genre_resultat(titre, transfert_genres);
        set_titleType_resultat(titre, transfert_titleType);
        set_annee_resultat(titre, transfert_startYear);
        set_averageRating_titre(titre,transfert_averageRating);
        set_numVotes_titre(titre,transfert_numVotes);
        set_ligne_titre(titre,transfert_lineNumber);
        // Ajouter le titre aux résultats
        ajouterTitre(resultats, titre);
        detruire_titre(titre);
        free(transfert_primaryTitle);
        free(transfert_tconst);
        free(transfert_genres);
        free(transfert_titleType);
    }
    return resultats;
}

// Envoie de la variable index d'evaluation
void envoie_index_Evaluation(int index, int socket_id){
    // Envoi de l'index a evaluer 
    int noctets = write(socket_id, &index, sizeof(int));
    if (noctets < sizeof(int)) {
        printf("Problème lors de l'écriture\n");
        exit(1);
    }
}

// Reception de l'index d'evaluation
int reception_index_Evaluation(int socket_id)
{
    int index;

    // Lecture de l'index
    int noctets = read(socket_id, &index, sizeof(int));
    if (noctets < sizeof(int)) {
        printf("Problème lors de la lecture\n");
        exit(1);
    }
    return index;
}

// Envoi de la cote d'evaluation
void envoie_cote_Evaluation(t_titreCherches resultat,int index, int socket_id)
{
    // Aller chercher le tconst
    char* tconst_titre=get_tconst_resultat(resultat,index);
    // Lecture de la taille de tconst
    int taille_tconst_titre = strlen(tconst_titre) + 1;
    // Envoi de la taille de tconst
    int noctets = write(socket_id, &taille_tconst_titre, sizeof(int));
    if (noctets < sizeof(int)) {
    printf("Problème lors de l'écriture\n");
        exit(1);
    }
    // Envoi du resultat tconst_titre
    noctets = write(socket_id, tconst_titre, taille_tconst_titre * sizeof(char));
    if (noctets < taille_tconst_titre * sizeof(char)) {
        printf("Problème lors de l'écriture\n");
        exit(1);
    }
    // Aller chercher l'averageRating_titre
    double averageRating_titre = get_averageRating_resultat(resultat,index);
    noctets = write(socket_id, &averageRating_titre, sizeof(double));
    if (noctets < sizeof(int)) {
        printf("Problème lors de l'écriture\n");
         exit(1);
    }
    // Aller chercher le numVotes_resultat
    int numvote_titre = get_numVotes_resultat(resultat,index);
    noctets = write(socket_id, &numvote_titre, sizeof(int));
    if (noctets < sizeof(int)) {
        printf("Problème lors de l'écriture\n");
        exit(1);
    }
}

// Reception de la cote d'evaluation
void reception_cote_Evaluation(int socket_id)
{
    // Lecture de la taille de tconst
    int taille_tconst_titre;
    int noctets = read(socket_id, &taille_tconst_titre, sizeof(int));
    if (noctets < sizeof(int)) {
        printf("Problème lors de la lecture\n");
        exit(1);
    }

    // Envoi de tconst_titre
    char* transfert_tconst_titre = (char*)malloc(taille_tconst_titre * sizeof(char));
    noctets = read(socket_id, transfert_tconst_titre, taille_tconst_titre * sizeof(char));
    if (noctets < taille_tconst_titre * sizeof(char)) {
        printf("Problème lors de la lecture\n");
        exit(1);
    }
    
    // Lecture de averageRating
    double transfert_averageRating_titre;
    noctets = read(socket_id, &transfert_averageRating_titre, sizeof(double));
    if (noctets < sizeof(double)) {
        printf("Problème lors de la lecture\n");
        exit(1);
    }
    
    // Lecture de numVotes
    int transfert_numVotes_titres;
    noctets = read(socket_id, &transfert_numVotes_titres, sizeof(int));
    if (noctets < sizeof(int)) {
        printf("Problème lors de la lecture\n");
        exit(1);
    }
printf("\nTitre : %s\n", transfert_tconst_titre);
printf("Moyenne des évaluations : %.1lf\n", transfert_averageRating_titre);
printf("Nombre de votes : %d\n", transfert_numVotes_titres);
free(transfert_tconst_titre);
}

// Comm-HLR10: Le client envoie au serveur l'évaluation de l'utilisateur
void envoie_note_Evaluation(double nouvelle_note, int socket_id)
{
    // Envoi de la nouvelle note 
    int noctets = write(socket_id, &nouvelle_note, sizeof(double));
    if (noctets < sizeof(double)) {
        printf("Problème lors de l'écriture dans le FIFO\n");
        exit(1);
    }
}

// Comm-HLR11: Le serveur reçoit l'évaluation
double reception_note_Evaluation(int socket_id)
{
    double nouvelle_note;
    // Lecture de la nouvelle note
    int noctets = read(socket_id, &nouvelle_note, sizeof(double));
    if (noctets < sizeof(double)) {
        printf("Problème lors de la lecture\n");
        exit(1);
    }
    return nouvelle_note;
}

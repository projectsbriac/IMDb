/**
 * Module resultat - resultat.h
 *
 * Par : Nicolas Verreault et Briac Samson-tessier
 * Date : Mai 2023
 *
 * Le module resultat est responsable de gérer la structure de données qui permet
 * de contenir tous les arguments de recherche reçus par le programme principal.
 * Il définit le type t_critere qui contient les critères de recherches.
 */

#ifndef RESULTAT_H
#define RESULTAT_H
#define _CRT_SECURE_NO_WARNINGS 

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * Encapsulation de la structure entreeTitre
 * Les champs sont accessibles seulement à l'intérieur du module resultat.c
 */
typedef struct entreeTitre* t_entree;

/**
 * Constructeur de entreeTitre
 *
 * Cette fonction publique permet d'allouer dynamiquement une structure de critères.
 * Elle place des valeurs par défaut dans les membres de celle-ci.
 *
 * Retour : pointeur vers la structure allouée
 */
t_entree creer_titre(void);

/**
 * Destructeur de entreeTitres
 *
 * Cette fonction publique permet de libérer une structure titres entreeTitre de la mémoire
 *
 * entreeTitre : la structure à libérer
 */
void detruire_titre(t_entree entreeTitre);

/**
 * Encapsulation de la structure titreCherches
 * Les champs sont accessibles seulement à l'intérieur du module resultat.c
 */
typedef struct titreCherches* t_titreCherches;
/**
 * Rajout de titre
 *
 * Cette fonction publique permet d'ajouter une structure de titres.
 * Elle place des valeurs par défaut dans les membres de celle-ci.
 *
 *  * titreCherches : la structure à ajouter
 */

void ajouterTitre(t_titreCherches resultats,t_entree titre);
/**
 * Constructeur de titreCherches
 *
 * Cette fonction publique permet d'allouer dynamiquement une structure de critères.
 * Elle place des valeurs par défaut dans les membres de celle-ci.
 *
 * Retour : pointeur vers la structure allouée
 */
t_titreCherches creer_listeTitres(void);

/**
 * Destructeur de liste de titres cherches
 *
 * Cette fonction publique permet de libérer une structure titres entreeTitre de la mémoire
 *
 * titreCherches : la structure à libérer
 */
void detruire_listeTitres(t_titreCherches titreCherches);
/**
 * ajout de la liste de titres dans un fichier resultat.tsv
 *
 * Cette fonction publique permet de ;odifier le fichier resultat.tsv pour ajouter la liste des titres
 *
 * resultat : liste de titre
 */
void TitresFichier(t_titreCherches resultats);
    // Ouvrir le fichier en mode écriture)


/**
 * Mutateurs Tconst
 *
 * Permet d'ajouter tcsonst à la structure titres
 *
 * titres : la structure à modifier
 * tconst : l'année à ajouter
 */
void set_tconst_resultat(t_entree titres,char *tconst);
/**
 * Mutateurs titre
 *
 * Permet d'ajouter titre à la structure titres
 *
 * titres : la structure à modifier
 * primaryTitle : titre a ajouter
 */
void set_titre_resultat(t_entree titres,char *primaryTitle);
/**
 * Mutateurs genres
 *
 * Permet d'ajouter genres à la structure titres
 *
 * titres : la structure à modifier
 * genres : genres a ajouter
 */
void set_genre_resultat(t_entree titres,char *genres);
/**
 * Mutateurs titleType
 *
 * Permet d'ajouter titleType à la structure titres
 *
 * titres : la structure à modifier
 * titleType : type de film a ajouter
 */
void set_titleType_resultat(t_entree titres, char *titleType);
/**
 * Mutateurs annee
 *
 * Permet d'ajouter startYear à la structure titres
 *
 * titres : la structure à modifier
 * startYear : type de film a ajouter
 */
void set_annee_resultat(t_entree titres, int startYear);
/**
 * Ajout de cote aux films
 *
 * Permet d'ajouter une cote et le nb de votes à la structure titres
 *
 * titres : la structure à modifier
 * coteFilm : La cote du film
 * nbVotes : Nombre de votes pour le film
 */
void ajout_Cote(t_entree titres, double coteFilm, int nbVotes);
/**
 * Mutateur de la moyenne de cote aux films
 *
 * Permet d'ajouter une cote à la structure titres
 *
 * titres : la structure à modifier
 * averageRating : La cote du film
 */
void set_numVotes_resultat(t_titreCherches resultat, int numVotes,int index);
/**
 * Mutateur du nombre de votes aux films
 *
 * Permet d'ajouter le nombre de votes à la structure titres
 *
 * titres : la structure à modifier
 * numVotes : La cote du film
 */
void set_averageRating_resultat(t_titreCherches resultat, double averageRating,int index);

/* OBSERVATEURS DES Resultats */

/**
 * Observateur de tconst
 *
 * Permet d'obtenir le tconst de la structure titres
 *
 * titres : la structure à observer
 *
 * Retour : chaine de caractères constituant le titre de la structure donnée
 *          en paramètre
 */
char* get_tconst_resultat(t_titreCherches resultat,int index);
/**
 * Observateur de titre
 *
 * Permet d'obtenir le titre de la structure titres
 *
 * titres : la structure à observer
 *
 * Retour : chaine de caractères constituant le titre de la structure donnée
 *          en paramètre
 */
char* get_titre_resultat(t_titreCherches resultats,int index);
/**
 * Observateur de genre
 *
 * Permet d'obtenir le genre de la structure titres
 *
 * titres : la structure à observer
 *
 * Retour : chaine de caractères constituant le titre de la structure donnée
 *          en paramètre
 */
char* get_genre_resultat(t_titreCherches resultats,int index);
/**
 * Observateur de titleType
 *
 * Permet d'obtenir le titleType de la structure titres
 *
 * titres : la structure à observer
 *
 * Retour : chaine de caractères constituant le titre de la structure donnée
 *          en paramètre
 */
char* get_titleType_resultat(t_titreCherches resultats,int index);
/**
 * Observateur de anne
 *
 * Permet d'obtenir l annee de la structure titres
 *
 * titres : la structure à observer
 *
 * Retour : chaine de caractères constituant le titre de la structure donnée
 *          en paramètre
 */
int get_annee_resultat(t_titreCherches resultats,int index);
/**
 * Observateur de averageRating
 *
 * Permet d'obtenir averageRating de la structure titres
 *
 * titres : la structure à observer
 *
 * Retour : chaine de caractères constituant le titre de la structure donnée
 *          en paramètre
 */
double get_averageRating_resultat(t_titreCherches resultats,int index);
/**
 * Observateur de numVotes
 *
 * Permet d'obtenir numVotes de la structure titres
 *
 * titres : la structure à observer
 *
 * Retour : rien
 */
int get_numVotes_resultat(t_titreCherches resultats,int index);
/**
 * Mutateur de nombres de votes pour un titre
 *
 * Permet de muter le nombre de votes pour un titre 
 *
 * titres : la structure a laquelle on ajoute un nombre de vote
 * numVotes : nombre de votes 
 *
 * Retour : rien
 */
void set_numVotes_titre(t_entree titre, int numVotes);

/**
 * Mutateur de moyenne d'une note a un titre de film
 *
 * Permet de muter une nouvelle moyenne de cotre dans la structure de titres
 *
 * titres : la structure a laquelle on ajoute un nombre de vote
 * averageRating : moyenne de votes 
 *
 * Retour : rien
 */
void set_averageRating_titre(t_entree titre, double averageRating);

/**
 * Observateur du nombre de titres de film
 *
 * Fonction permettant de recupere le nombre de votes dans la 
 * structure de titre desire par l'index
 *
 * t_titreCherches : structure de titres  
 *
 * Retour : le nombre de titres
 */
int get_nbTitres_resultat(t_titreCherches resultat);

/**
 * Observateur du numero de ligne d'un titre de film
 *
 * Fonction permettant de recupere le numero de ligne de titres 
 * dans la structure de resultat
 *
 * t_titreCherches : structure de titres 
 * index : variable index 
 *
 * Retour : le numero de ligne d'un titre
 */
int get_ligne_resultat(t_titreCherches resultat,int index);

/**
 * Mutateur du numero de ligne d'un titre de film
 *
 * Fonction permettant d'envoyer le numero de ligne de titres 
 * dans la structure de resultat
 *
 * t_titreCherches : structure de titres 
 * numLigne : le numero de ligne
 * index : variable index 
 *
 * Retour : rien
 */
void set_ligne_resultat(t_titreCherches resultat,int numLigne,int index);

/**
 * Ajout d'une note au classement moyen d'un titre
 *
 * Fonction permettant d'obtenir le nouveau classement moyen d'un titre d'un film
 *
 * t_titreCherches : structure de titres 
 * index : variable index 
 * nouvelle_note : nouvelle note d'un film
 *
 * Retour : rien
 */
void classement_moyen(t_titreCherches resultat,int index ,double nouvelle_note);

/**
 * Ajout d'un numero de ligne a un titre
 *
 * Fonction permettant d'ajouter le numero de ligne a un titre
 *
 * t_titreCherches : structure de titres 
 * lineNumber : numero d'une ligne
 *
 * Retour : rien
 */
void set_ligne_titre(t_entree titre,int lineNumber);
#endif
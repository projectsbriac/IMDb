/**
 * Module de tests unitaires - testunitaire.h
 *
 * Par : Nicolas Verreault et Briac Samson-Tessier
 * Date : Juin 2023
 *
 * Ce module sert a tester le bon fonctionnement du programme du laboratoire 2. Plus specifiquement, 
 * il sert a s'assurer du bon fonctionnement des modules recherche.c et recherche.h.
 */


#ifndef TESTUNITAIRE_H
#define TESTUNITAIRE_H
#define _CRT_SECURE_NO_WARNINGS 

#include "recherche.h"
#include "resultat.h"
#include "imdb.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "assert.h"

int test_compare_strings(const char* str1, const char* str2);
void test_creer_detruire_critere(void);
void test_mutateurs_observateurs_critere(void);
void test_set_intervalle_annees(void);

#endif
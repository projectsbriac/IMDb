/**
 * Module imdb - imdb.h
 *
 * Par : Nicolas Verreault et Briac Samson-tessier
 * Date : Juin 2023
 *
 * Ce module est responsable des fonctions pour l'exploration de la base de données de titres et de cotes
 * dans des fichiers situe dans le repertoire parents (title_basics.tsv et title_ratings.tsv)
 * ce module va comparer des critères de recherche avec les lignes du fichier title_basics.tsv
 * et ajouter les titres dans la structure resultat du module resultat.c.
 * imdb.c contient egalement une fonction pour comparer les tcsont avec les lignes du fichier title_ratings.tsv
 * pour ainsi, pouvoir ajouter les cotes au tconst correspond au titres.
 * 
 */

#ifndef IMDB_H
#define IMDB_H
#define _CRT_SECURE_NO_WARNINGS 

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include "resultat.h"
#include "recherche.h"

#define LOG(string) \
    printf(string); \
    fflush(stdout) 


#define FLOG(string, ...) \
    printf(string, __VA_ARGS__); \
    fflush(stdout) 



/**
 * Exploration de la base de données de titres
 *
 * Cette fonction publique du module imdb permet d'explorer la base de données de titres
 * en utilisant les critères de recherche fournis.
 *
 * critere : les critères de recherche
 */
t_titreCherches explorer_base_de_donnees(t_critere critere);


/**
 * Exploration de la base de données des cotes
 *
 * Cette fonction publique du module imdb permet d'explorer la base de données des cotes
 * en utilisant les critères de recherche fournis.
 *
 * resultat : La structure de liste de titre
 */
void explorer_base_de_donnees_cotes(t_titreCherches resultat);
/**
 * maj de la base de donnees de cotes
 *
 * Cette fonction publique du module imdb permet de mettre a jour la base de donnees de 
 * cotes 
 *
 * ligne_a_supprimer : Ligne a supprimer choisi par l'utilisateur
 * nouvelle_id : nouveau numero d'identification de ligne
 * nouvelle_cote : nouvelle cote choisi par l'utilisateur 
 */
// Labo3 Serveur-HLR07: Génération d'un nouveau fichier de cotes de classement
void maj_fichier_cote(t_titreCherches resultat,int index);

/**
 * vider variable numLigne
 *
 * Cette fonction publique du module imdb permet de mettre a zero la variable numLigne
 *
 * Retour : rien
 */
void vider_numligne(void);
#endif
/**
 * Module communication - communication.h
 *
 * Par : Nicolas Verreault et Briac Samson-tessier
 * Date : Juillet 2023
 *
 * Ce module est responsable des fonctions pour la communication entre les fichier client.c
 * et le serveur.c. Toutes les fonctions presentes servent a ouvrir un fifo, communiquer 
 * l'information et fermer le fifo.
 *
 */

#ifndef COMMUNICATION_H
#define COMMUNICATION_H
#define _CRT_SECURE_NO_WARNINGS 

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <pthread.h>

#include "recherche.h"
#include "resultat.h"

#define MON_FIFO "/tmp/un_fifo"
#define FIELD_ID_SIZE 10 
#define PORT 8080
#define PORTB 8090

/**
 * Observateur de t_critere
 *
 * Cette fonction publique permet d'aller recuperer les criteres dans le FIFO et de placer
 * les valeurs dans des variables dynamiques, qui ont ete allouee dynamiquement dans cette
 * fonction
 *
 * Retour : pointeur vers la structure allouée
 */
t_critere recuperer_Critere_Recherche(int socket_id);

/**
 * Envoie de donnees de criteres par FIFO
 *
 * Cette fonction publique permet d'envoyer les criteres de recherches par FIFO 
 * 
 * Titre : Variable Titre envoyee
 * Genres : Variable Genres envoyee
 * Annees : Variable Annees envoyee
 * Categorie : Variable Categorie envoyee
 *
 * Retour : Rien 
 */
void envoyer_Critere_Recherche(t_critere critere, int socket_id);

/**
 * Envoie de donnees des resultats par FIFO
 *
 * Cette fonction publique permet d'envoyer les resultats de recherches par FIFO 
 * 
 * t_titreCherches : structure de resultats de titres 
 *
 * Retour : Rien 
 */
void envoie_resultats_recherche(t_titreCherches resultat, int socket_id);

/**
 * Reception des donnees des resultats de recherches par FIFO
 *
 * Cette fonction publique permet de recevoir la structure de resultat des titres par FIFO 
 * 
 * t_titreCherches : structure de la liste de titres
 *
 * Retour : Rien 
 */
t_titreCherches reception_resultats_recherche(int socket_id);

/**
 * Envoie d'index par FIFO
 *
 * Cette fonction publique permet d'envoyer une variable index par FIFO 
 * 
 * index : variable index
 *
 * Retour : Rien 
 */
void envoie_index_Evaluation(int index, int socket_id);

/**
 * Reception d'index par FIFO
 *
 * Cette fonction publique permet d'aller chercher une variable index par FIFO 
 * 
 * index : variable index
 *
 * Retour : la variable index
 */
int reception_index_Evaluation(int socket_id);

/**
 * Envoie d'une cote par FIFO
 *
 * Cette fonction publique permet d'envoyer une variable cote d'evaluation par FIFO 
 * 
 * t_titreCherches : structure de la liste de titres resultats
 * index : variable index
 *
 * Retour : Rien 
 */
void envoie_cote_Evaluation(t_titreCherches resultat,int index, int socket_id);

/**
 * Reception d'une cote par FIFO
 *
 * Cette fonction publique permet de recevoir une variable cote d'evaluation par FIFO 
 * 
 * Retour : Rien 
 */
void reception_cote_Evaluation(int socket_id);

/**
 * Envoie d'une note par FIFO
 *
 * Cette fonction publique permet d'envoyer une variable note d'evaluation par FIFO 
 * 
 * nouvelle_note : variable nouvelle_note pour film
 *
 * Retour : Rien 
 */
void envoie_note_Evaluation(double nouvelle_note, int socket_id);

/**
 * Reception d'une note par FIFO
 *
 * Cette fonction publique permet de recevoir une variable note d'evaluation par FIFO 
 *
 * Retour : note d'evaluation
 */
double reception_note_Evaluation(int socket_id);

#endif
## Description
Le but est de ce programme en C est de rechercher le titre d'un film avec son année, le/les genres, ainsi que la catégorie. Mais également de pouvoir noter les différents films de plusieurs clients en simultané et de mettre la base de données à jour en fonction des nouvelles notes attribuées.
Pour se faire on a utilisé un protocole de communication client/serveur socket et du multithreading et une protection mutex/sémaphore pour éviter tout problème lié à la modification de la base de données en simultané.
à noter que pour faire fonctionner le programme il faut au préalable télécharger les bases de données du site Imdb via le lien suivant :
https://datasets.imdbws.com/

L'utilisateur doit entrer une ligne de commande a partir du dossier Debug tel que :
./client -t "The Good" -g "Western" -a "1960:1970" -c "movie"

Le serveur lui doit tourner en permanence pour effectuer les requêtes de tout les clients:
./serveur

Le premier document se nomme title_basics, il s'y retrouve les variables suivantes :
- tconst : Identifiant unique du film,
- primaryTitle : Titre du film,
- startYear : Annee de parution,
- genres : Genres du film,
- titleType : Categorie du titre,

Le second document se nomme title_ratings, il s'y retrouve les variables suivantes :
- tconst : Identifiant unique du film,
- averageRating : Moyenne de la cote sur 10,
- numVotes : Nombre de votes du film.

## Installation
Le programme fonctionne a l'aide de Visual Studio Code, en language C. Pour pouvoir le faire fonctionner, le systeme d'exploitation doit etre Linux. 

De plus, vous devrez installer les bibliotheques "title_bascis.tsv", ainsi que "title_ratings.tsv" dans le repertoire racine du laboratoire pour que le programme soit capable d'aller chercher dans leurs banques de donnees. 

## Usage
Dans ce programme, les programmes suivants sont utilises :

- client.c : Ce module est le programme principal. c'est ici que le client envoie ses différentes requêtes au serveur.
- serveur.c : Ce module est le programme principal. Le serveur tourne en permanence et execute les différentes requêtes du client.

- resultat.c : Ce module recoit les donnees de imdb.c et les imprimes dans les fichier "title_basics.tsv" ainsi que dans "title_ratings.tsv". 

- recherche.c : Ce module est responsable de gerer la structure de donnees qui permet de contenir tous les arguments de recherche recus par le programme principal. 

- imdb.c : Ce module contient les fonctions pour l'exploration de la base de donnees de titres et de cotes dans les fichier situees dans le repertoire parent (title_basics.tsv et title_ratings.tsv). Ce module va comparer les criteres de recherche avec les lignes du fichier title_basics.tsv et ajouter les titres dans la structure resultat du module resultat.c.

- communication.c : module contennant les fonctions pour la communication entre le client et le serveur.

- makeFile : Ce module sert a initier le programme lorsque l'utilisateur tape "make". De plus, il est possible d'entrer la commande "make clean" pour effacer le dossier Debug et reinitialiser le programme. 

## Support
Pour du support, vous pouvez ecrire aux addresses electroniques suivantes :

- nicolas.verreault.2@ens.etsmtl.ca
- briac.samson-tessier.1@ens.etsmtl.ca

## Authors and acknowledgment
Autheurs du programme : 

- Briac Samson-Tessier
- Nicolas Verreault 
- Enseignants ELE216
